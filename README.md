# h2-perftest

Script to demonstrate a performance problem with h2 database version 1.4.197

## Steps do reproduce:
1. mvn package
2. java -cp lib/h2-1.4.196.jar:target/h2-perftest-0.0.1-SNAPSHOT.jar ch.meemin.h2perftest.PerfTest .
3. java -cp lib/h2-1.4.197.jar:target/h2-perftest-0.0.1-SNAPSHOT.jar ch.meemin.h2perftest.PerfTest .
