package ch.meemin.h2perftest;

import java.io.File;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Random;

public class PerfTest {
	public static void main(String[] args) {
		Connection conn = null;
		try {

			String dbURL = "jdbc:h2:mem:perftest";
			String user = "sa";
			String pass = "";
			conn = DriverManager.getConnection(dbURL, user, pass);

			DatabaseMetaData dm = (DatabaseMetaData) conn.getMetaData();
			System.out.println("Driver name: " + dm.getDriverName());
			System.out.println("Driver version: " + dm.getDriverVersion());
			Statement stm = conn.createStatement();

			
			System.out.println("Creating table");
			long l1 = System.currentTimeMillis();
			stm.execute("CREATE TABLE T(ID INT PRIMARY KEY, TXT VARCHAR(200) DEFAULT NULL)");
			long l2 = System.currentTimeMillis();
			System.out.println("Created table: "+(l2-l1)+" ms");
			
			System.out.println("Inserting 10000 rows");
			 l1 = System.currentTimeMillis();
			for(int i =0;i<10000;i++) {
				stm.execute("insert into T VALUES("+i+",null)");
			}
			l2 = System.currentTimeMillis();
			System.out.println("Inserted: "+(l2-l1)+" ms");

			System.out.println("Update all rows");
			l1 = System.currentTimeMillis();
			stm.execute("update T set txt = 'Foo'");
			l2 = System.currentTimeMillis();
			System.out.println("Updated: "+(l2-l1)+" ms");


			System.out.println("Altering table");
			l1 = System.currentTimeMillis();
			stm.execute("alter table T alter column TXT VARCHAR(220)");
			l2 = System.currentTimeMillis();
			System.out.println("Altered: "+(l2-l1)+" ms");

			System.out.println("Update all rows");
			l1 = System.currentTimeMillis();
			stm.execute("update T set txt = 'Bar'");
			l2 = System.currentTimeMillis();
			System.out.println("Updated: "+(l2-l1)+" ms");


		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				if (conn != null && !conn.isClosed()) {
					conn.close();
				}
			} catch (SQLException ex) {
				ex.printStackTrace();
			}
		}
	}
}
